package com.devcamp.springrestapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RequestMapping("/")
@RestController
public class InvoiceItems {
    private String id;
    private String decs;
    private int qty;
    private double unitPrice;

    public InvoiceItems() {
    }

    public InvoiceItems(String id, String decs, int qty, double unitPrice) {
        this.id = id;
        this.decs = decs;
        this.qty = qty;
        this.unitPrice = unitPrice;
    }

    public String getId() {
        return id;
    }

    public String getDecs() {
        return decs;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public double getTotal() {
        return qty * unitPrice;
    }

    @Override
    public String toString() {
        return "InvoiceItems [id=" + id + ", decs=" + decs + ", qty=" + qty + ", unitPrice=" + unitPrice + "]";
    }

    @GetMapping("/invoices")
    public ArrayList<InvoiceItems> InvoiceList() {
        ArrayList<InvoiceItems> Invoices = new ArrayList<InvoiceItems>();

        InvoiceItems invoive01 = new InvoiceItems("BUNCHA", "Bun cha ngon nham", 20, 50000);
        InvoiceItems invoive02 = new InvoiceItems("BANH XEO", "Banh Xeo Ma Bong Lam", 100, 200000);
        InvoiceItems invoive03 = new InvoiceItems("THITKHOTAU", "Thit kho Tau", 10, 100000);

        System.out.println(invoive01.getTotal());
        System.out.println(invoive01.toString());

        System.out.println(invoive02.getTotal());
        System.out.println(invoive02.toString());

        System.out.println(invoive03.getTotal());
        System.out.println(invoive03.toString());

        Invoices.add(invoive01);
        Invoices.add(invoive02);
        Invoices.add(invoive03);

        return Invoices;
    }

}
